The assignment.py file uses boto3 library to fetch records from route 53.Python was choosen based on the simplicity of boto3 library.

The records fetched from route53 is pushed to Redis datastore.Redis is an in-memory database that is modeled on Key-Value pair.

https://github.com/antirez/redis

NOTE: Redis is running as a docker container on a ec2 instance

The script runs every minute as cron. ( Could be converted as Lambda function )
*  *    * * *   root    /usr/bin/python3  /opt/assignment.py

For the WebUI i have gone with an opensource solution called webdis

https://github.com/nicolasff/webdis

It is simple to use and provides a HTTP interface

NOTE: Webdis is running as a docker container on ec2 instance in network mode and talks to redis.
