import redis
import boto3
client = boto3.client('route53')
records = {}
a_list = []
response1 = client.list_resource_record_sets(HostedZoneId='Z00100372ZZ05IXCN6LDK') #Can pull zone id from env variables
for i in response1['ResourceRecordSets']:
        j = i['Name'], i['ResourceRecords'][0]['Value']
        a_list.append(tuple((j[0],j[1])))
records.update(a_list)
#print(records)

conn = redis.Redis('localhost')
conn.hmset("route_53", records)
conn.hgetall("route_53")
